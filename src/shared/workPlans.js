export const workPlansEnum={
    work:'Work',
    workFromHome:'Work from home',
    business:'Business trip',
    holiday:'Holiday',
    publicHoliday:'Public holiday'
};
